package studio.sun.cybirst.Utils;

/**
 * Created by Sun on 10/11/2017.
 */

public class Config {
    //URL to our login.php file
    public static final String BASE_API = "https://api.mathover.com";
//    public static final String BASE_API = "http://192.168.43.219:8080";
    public static final String LOGIN_URL = BASE_API + "/api/v1/auth/login";
    public static final String UPLOAD_URL = BASE_API + "/api/v1/upload-image";
    public static final String QUESTION_URL = BASE_API + "/api/v1/questions";
    public static final String getQuestionApiString(int page){
        return QUESTION_URL + "?page="+ page +"&size=10&type=IMG";
    };
    public static final String getQuestionApiUpvote(String access_id){
        return QUESTION_URL + "/"+ access_id +"/upvote";
    };
    public static final String getQuestionApiDownvote(String access_id){
        return QUESTION_URL + "/"+ access_id +"/downvote";
    };
    public static final String getAnswerApiString(String access_id){
        return QUESTION_URL + "/"+ access_id +"/answers";
    };

    //CODE OF UploadAnswerActivity
    public static final int UAA_CODE = 0;
    public static final int UQA_CODE = 1;

    public static final String LOGIN_SUCCESS = "success";

    //Keys for Sharedpreferences
    //This would be the name of our shared preferences
    public static final String SHARED_PREF_NAME = "mathover";

    //This would be used to store the email of current logged in user
    public static final String BEAR_TOKEN = "Authorization";

    //We will use this to store the boolean in sharedpreference to track user is loggedin or not
    public static final String LOGGEDIN_SHARED_PREF = "loggedin";
}
