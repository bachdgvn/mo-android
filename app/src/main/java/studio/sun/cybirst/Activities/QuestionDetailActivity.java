package studio.sun.cybirst.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import studio.sun.cybirst.CustomViews.NonScrollListView;
import studio.sun.cybirst.Entities.Answer;
import studio.sun.cybirst.Entities.AnswersAdapter;
import studio.sun.cybirst.Entities.Question;
import studio.sun.cybirst.Entities.Tag;
import studio.sun.cybirst.R;
import studio.sun.cybirst.Utils.Config;

public class QuestionDetailActivity extends AppCompatActivity {
    Question question;
    ArrayList<Answer> answers = new ArrayList<Answer>();
    SharedPreferences preferences;
    AnswersAdapter answersAdapter;
    TextView tvQuestionNumAnswer;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        TextView btnOpenAnswerDialog = (TextView)findViewById(R.id.btnOpenAnswerDialog);

        //GET DATA FROM INTENT
        Intent intent = getIntent();
        final String jsonString = intent.getStringExtra("QUESTION");
        preferences = QuestionDetailActivity.this.getSharedPreferences(Config.SHARED_PREF_NAME, MODE_PRIVATE);

        btnOpenAnswerDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itentAnswerDialog = new Intent(QuestionDetailActivity.this, UploadAnswerActivity.class);
                itentAnswerDialog.putExtra("QUESTION", jsonString);
                startActivityForResult(itentAnswerDialog, Config.UAA_CODE);
            }
        });

        try {
            JsonParser parser = new JsonParser();
            JsonElement mJson =  parser.parse(jsonString);
            question = gson.fromJson(mJson, Question.class);

            TextView tvTitle = (TextView) findViewById(R.id.tvDTitle);
            TextView tvQuestionOwnerFullName = (TextView) findViewById(R.id.tvDQuestionOwnerFullName);
            ImageView ivQuestionOwnerAvatar = (ImageView) findViewById(R.id.ivDQuestionOwnerAvatar);
            ImageView ivContent = (ImageView) findViewById(R.id.ivDContent);
            tvQuestionNumAnswer = (TextView) findViewById(R.id.tvDQuestionNumAnswer);
            LinearLayout llTags = (LinearLayout) findViewById(R.id.llDTags);

            tvTitle.setText(question.getTitle());
            tvQuestionOwnerFullName.setText(question.getUser().getFullname());
            tvQuestionNumAnswer.setText(question.getNum_answer() + " trả lời");

            Picasso.with(this).load(question.getUser().getAvatar()).fit().centerCrop()
                    .error(R.mipmap.ic_launcher)
                    .into(ivQuestionOwnerAvatar);

            Picasso.with(this).load(question.getContent()).fit().centerCrop()
                    .error(R.mipmap.ic_launcher)
                    .into(ivContent);

            ArrayList<Tag> arrayList = question.getTags();
            int index = 0;
            llTags.removeAllViews();
            for (Tag tag : arrayList) {
                TextView tvTag = new TextView(this);
                tvTag.setId(index);
                tvTag.setText("(" + tag.getContent() + ")");
                tvTag.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                llTags.addView(tvTag);
                index++;
            }
            //GET ANSWERS
            initAnswers();
        }catch (Exception e){
            Log.e("error", e.toString());
        }
    }

    public void initAnswers(){
        NonScrollListView answerListView = (NonScrollListView) findViewById(R.id.lvAnswers);;
        answersAdapter = new AnswersAdapter(QuestionDetailActivity.this, answers);
        answerListView.setAdapter(answersAdapter);
        loadDataFromApi(question.getAccess_id());
    }

    public void loadDataFromApi(String access_id) {
        Log.i("api", Config.getAnswerApiString(access_id));
        final RequestQueue queue = Volley.newRequestQueue(QuestionDetailActivity.this);
        JsonArrayRequest getRequest = new JsonArrayRequest(Config.getAnswerApiString(access_id),
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        JsonParser parser = new JsonParser();
                        try {
                            JSONArray jsonArray = response;
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JsonElement mJson =  parser.parse(jsonObject.toString());
                                Answer answer = gson.fromJson(mJson, Answer.class);
                                answers.add(answer);
                            }
                            answersAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            Log.e("error", e.toString());
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(Config.BEAR_TOKEN, preferences.getString(Config.BEAR_TOKEN, ""));
                return headers;
            };
        };
        queue.add(getRequest);
    }

    // This method is called when the second activity finishes
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check that it is the UploadAnswerActivity with an OK result
        if (requestCode == Config.UAA_CODE) {
            if (resultCode == RESULT_OK) { // Activity.RESULT_OK

                // get String data from Intent
                String returnString = data.getStringExtra("newAnswer");
                Answer answer = gson.fromJson(returnString, Answer.class);
                answers.add(0, answer);
                question.setNum_answer(question.getNum_answer() + 1);
                tvQuestionNumAnswer.setText(question.getNum_answer() + " trả lời");
                answersAdapter.notifyDataSetChanged();
            }
        }
    }
}
