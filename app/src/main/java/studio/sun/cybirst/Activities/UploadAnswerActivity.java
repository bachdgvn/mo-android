package studio.sun.cybirst.Activities;

import android.content.ActivityNotFoundException;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.IOException;

import id.zelory.compressor.Compressor;
import studio.sun.cybirst.Entities.Question;
import studio.sun.cybirst.R;
import studio.sun.cybirst.Utils.Config;

public class UploadAnswerActivity extends AppCompatActivity {
    ImageView btnOpenGalley;
    ImageView ivSelectedImage;
    Button btnSubmitAnswer;
    Question currentQuestion;
    TextView tvActionDescription;
    EditText etAnswerTitle;
    String path;
    String answerTitle;
    SharedPreferences preferences;
    AVLoadingIndicatorView aviUploadAnswer;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_answer);
        //GET DATA FROM INTENT
        Intent intent = getIntent();
        final String jsonString = intent.getStringExtra("QUESTION");
        JsonParser parser = new JsonParser();
        JsonElement mJson =  parser.parse(jsonString);
        currentQuestion = gson.fromJson(mJson, Question.class);

        preferences = UploadAnswerActivity.this.getSharedPreferences(Config.SHARED_PREF_NAME, MODE_PRIVATE);

        tvActionDescription = (TextView) findViewById(R.id.tvActionDescription);
        ivSelectedImage = (ImageView) findViewById(R.id.ivSelectedImage);
        btnOpenGalley = (ImageView) findViewById(R.id.btnOpenGalley);

        btnSubmitAnswer =(Button)findViewById(R.id.btnSubmitAnswer);
        btnSubmitAnswer.setVisibility(View.GONE);
        btnSubmitAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage(path);
            }
        });
        etAnswerTitle = (EditText) findViewById(R.id.etAnswerTitle);

        btnOpenGalley.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent fintent = new Intent(Intent.ACTION_GET_CONTENT);
            fintent.setType("image/jpeg");
            try {
                startActivityForResult(fintent, 100);
            } catch (ActivityNotFoundException e) {
                Log.e("error", e.toString());
            }
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    path = getPathFromURI(data.getData());
                    ivSelectedImage.setImageURI(data.getData());

                    ivSelectedImage.setVisibility(View.VISIBLE);
                    btnSubmitAnswer.setVisibility(View.VISIBLE);
                    btnOpenGalley.setVisibility(View.GONE);
                    tvActionDescription.setVisibility(View.GONE);
                }
        }
    }

    private String getPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void uploadImage(String path){
        File f = new File(path);
        try{
            File compressedImageFile = new Compressor(this).compressToFile(f);

            Ion.getDefault(this).configure().setLogging("ion-upload", Log.DEBUG);

            aviUploadAnswer = (AVLoadingIndicatorView)findViewById(R.id.aviUploadAnswer);
            aviUploadAnswer.show();
            Ion.with(this)
                    .load(Config.UPLOAD_URL)
                    .setHeader("type", "answers")
                    .setTimeout(60 * 60 * 1000)
                    .setMultipartFile("fileInput", "image/jpeg", compressedImageFile)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                        if (e != null) {
                            Toast.makeText(getApplicationContext(), "Lỗi khi tải ảnh", Toast.LENGTH_LONG).show();
                            return;
                        }else{
                            addAnswer(result.get("src").getAsString());
                        }
                        }
                    });
        }catch (IOException e){
            Toast.makeText(getApplicationContext(), "Ảnh quá lớn.", Toast.LENGTH_LONG).show();
        }
    }

    private void addAnswer(String imageUrl){
        JsonObject json = new JsonObject();
        json.addProperty("title", etAnswerTitle.getText().toString());
        json.addProperty("content", Config.BASE_API + "/" + imageUrl);
        json.addProperty("type", "IMG");
        String token = preferences.getString(Config.BEAR_TOKEN, "");

        Ion.with(this)
            .load(Config.getAnswerApiString(currentQuestion.getAccess_id()))
            .setTimeout(60 * 60 * 1000)
            .setHeader(Config.BEAR_TOKEN, preferences.getString(Config.BEAR_TOKEN, ""))
            .setJsonObjectBody(json)
            .asJsonObject()
            .setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result) {
                    if (e != null) {
                        Toast.makeText(getApplicationContext(), "Lỗi khi tải lên câu trả lời!", Toast.LENGTH_LONG).show();
                        return;
                    }else{
                        Intent intent = new Intent();
                        String json = gson.toJson(result);
                        intent.putExtra("newAnswer", json);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            });
    }
}
