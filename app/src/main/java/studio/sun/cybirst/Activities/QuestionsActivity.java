package studio.sun.cybirst.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import studio.sun.cybirst.CustomViews.EndlessScrollListener;
import studio.sun.cybirst.Entities.Question;
import studio.sun.cybirst.Entities.QuestionsAdapter;
import studio.sun.cybirst.R;
import studio.sun.cybirst.Utils.Config;

public class QuestionsActivity extends AppCompatActivity {
    ArrayList<Question> questions = new ArrayList<Question>();
    SharedPreferences preferences;
    QuestionsAdapter questionsAdapter;
    Gson gson = new Gson();
    Boolean isFirstTimeLoadData = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);
        preferences = QuestionsActivity.this.getSharedPreferences(Config.SHARED_PREF_NAME, MODE_PRIVATE);

        ListView questionListView = (ListView) findViewById(R.id.lvQuestions);
        questionsAdapter = new QuestionsAdapter(this, questions);
        questionListView.setAdapter(questionsAdapter);
        loadDataFromApi(1);
        // Attach the listener to the AdapterView onCreate
        questionListView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView
                loadDataFromApi(page);
                isFirstTimeLoadData = false;
                // or loadDataFromApi(totalItemsCount);
                return true; // ONLY if more data is actually being loaded; false otherwise.
            }
        });

        questionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String json = gson.toJson(questions.get(i));
                Intent intent = new Intent(QuestionsActivity.this, QuestionDetailActivity.class);
                intent.putExtra("QUESTION", json);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        if(!isFirstTimeLoadData){
            ListView questionListView = (ListView) findViewById(R.id.lvQuestions);
            questionsAdapter = new QuestionsAdapter(this, questions);
            questionListView.setAdapter(questionsAdapter);
            loadDataFromApi(1);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_action_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnOpenAddQuestionDialog:
                Intent itentQuestionDialog = new Intent(QuestionsActivity.this, UploadQuestionActivity.class);
                startActivityForResult(itentQuestionDialog, Config.UQA_CODE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // This method is called when the second activity finishes
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check that it is the UploadQuestionActivity with an OK result
        if (requestCode == Config.UQA_CODE) {
            if (resultCode == RESULT_OK) { // Activity.RESULT_OK
                // get String data from Intent
                Intent intent = data;
                String returnString = intent.getStringExtra("newQuestion");
                Question question = gson.fromJson(returnString, Question.class);
                questions.add(0, question);
                questionsAdapter.notifyDataSetChanged();
            }
        }
    }

    // Append the next page of data into the adapter
    // This method probably sends out a network request and appends new data items to your adapter.
    public void loadDataFromApi(int page) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an page value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyDataSetChanged()`
        Log.i("api", Config.getQuestionApiString(page));
        final RequestQueue queue = Volley.newRequestQueue(QuestionsActivity.this);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, Config.getQuestionApiString(page), null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        JsonParser parser = new JsonParser();
                        try {
                            JSONArray jsonArray = response.getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JsonElement mJson =  parser.parse(jsonObject.toString());
                                Question question = gson.fromJson(mJson, Question.class);
                                questions.add(question);
                            }
                            questionsAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            Log.e("error", e.toString());
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(Config.BEAR_TOKEN, preferences.getString(Config.BEAR_TOKEN, ""));
                return headers;
            };
        };

        // add it to the RequestQueue
        queue.add(getRequest);
    }
}
