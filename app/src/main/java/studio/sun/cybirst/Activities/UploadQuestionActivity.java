package studio.sun.cybirst.Activities;

import android.content.ActivityNotFoundException;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;
import studio.sun.cybirst.Entities.Tag;
import studio.sun.cybirst.R;
import studio.sun.cybirst.Utils.Config;

public class UploadQuestionActivity extends AppCompatActivity {
    ImageView btnOpenQuestionGalley;
    ImageView ivSelectedQuestionImage;
    Button btnSubmitQuestion;
    TextView tvActionQuestionDescription;
    EditText etQuestionTitle;
    Spinner spTags;
    Tag selectedTag;
    String path;
    String questionTitle;
    SharedPreferences preferences;
    AVLoadingIndicatorView aviUploadQuestion;
    Gson gson = new Gson();
    ArrayList<Tag> tagArrayList = new ArrayList<Tag>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_question);

        tagArrayList.add(new Tag(1, "Toán lớp 1", ""));
        tagArrayList.add(new Tag(2, "Toán lớp 2", ""));
        tagArrayList.add(new Tag(3, "Toán lớp 3", ""));
        tagArrayList.add(new Tag(4, "Toán lớp 4", ""));
        tagArrayList.add(new Tag(5, "Toán lớp 5", ""));
        tagArrayList.add(new Tag(6, "Toán lớp 6", ""));
        tagArrayList.add(new Tag(7, "Toán lớp 7", ""));
        tagArrayList.add(new Tag(8, "Toán lớp 8", ""));
        tagArrayList.add(new Tag(9, "Toán lớp 9", ""));
        tagArrayList.add(new Tag(10, "Toán lớp 10", ""));
        tagArrayList.add(new Tag(11, "Toán lớp 11", ""));
        tagArrayList.add(new Tag(12, "Toán lớp 12", ""));
        tagArrayList.add(new Tag(13, "Ôn thi đại học", ""));

        preferences = UploadQuestionActivity.this.getSharedPreferences(Config.SHARED_PREF_NAME, MODE_PRIVATE);

        tvActionQuestionDescription = (TextView) findViewById(R.id.tvActionQuestionDescription);
        ivSelectedQuestionImage = (ImageView) findViewById(R.id.ivSelectedQuestionImage);
        btnOpenQuestionGalley = (ImageView) findViewById(R.id.btnOpenQuestionGalley);
        etQuestionTitle = (EditText) findViewById(R.id.etQuestionTitle);
        spTags = (Spinner) findViewById(R.id.spTags);

        btnSubmitQuestion =(Button)findViewById(R.id.btnSubmitQuestion);
        btnSubmitQuestion.setVisibility(View.GONE);
        btnSubmitQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questionTitle = etQuestionTitle.getText().toString().trim();
                if(questionTitle.length() > 0){
                    uploadImage(path);
                }else{
                    etQuestionTitle.setError( "Hãy mô tả rõ hơn về yêu cầu của bạn trong bức hình trên!" );
                }
            }
        });

        btnOpenQuestionGalley.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent fintent = new Intent(Intent.ACTION_GET_CONTENT);
            fintent.setType("image/jpeg");
            try {
                startActivityForResult(fintent, 100);
            } catch (ActivityNotFoundException e) {
                Log.e("error", e.toString());
            }
            }
        });

        ArrayAdapter<Tag> tagsAdapter = new ArrayAdapter<Tag>(this, R.layout.simple_spinner_item, tagArrayList);
        spTags.setAdapter(tagsAdapter);

        spTags.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Get select item
                int sid = spTags.getSelectedItemPosition();
                selectedTag = tagArrayList.get(sid);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    path = getPathFromURI(data.getData());
                    ivSelectedQuestionImage.setImageURI(data.getData());
                    ivSelectedQuestionImage.setVisibility(View.VISIBLE);
                    btnSubmitQuestion.setVisibility(View.VISIBLE);
                    btnOpenQuestionGalley.setVisibility(View.GONE);
                    tvActionQuestionDescription.setVisibility(View.GONE);
                }
        }
    }

    private String getPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void uploadImage(String path){
        File f = new File(path);
        try{
            File compressedImageFile = new Compressor(this).compressToFile(f);

            Ion.getDefault(this).configure().setLogging("ion-upload", Log.DEBUG);

            aviUploadQuestion = (AVLoadingIndicatorView)findViewById(R.id.aviUploadQuestion);
            aviUploadQuestion.show();
            Ion.with(this)
                .load(Config.UPLOAD_URL)
                .setHeader("type", "questions")
                .setTimeout(60 * 60 * 1000)
                .setMultipartFile("fileInput", "image/jpeg", compressedImageFile)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e != null) {
                            Toast.makeText(getApplicationContext(), "Lỗi khi tải ảnh", Toast.LENGTH_LONG).show();
                            return;
                        }else{
                            addQuestion(result.get("src").getAsString());
                        }
                    }
                });
        }catch (IOException e){
            Toast.makeText(getApplicationContext(), "Ảnh quá lớn.", Toast.LENGTH_LONG).show();
        }
    }

    private void addQuestion(String imageUrl){
        JsonObject json = new JsonObject();
        json.addProperty("title", questionTitle);
        json.addProperty("content", Config.BASE_API + "/" + imageUrl);
        json.addProperty("tag_id", selectedTag.getId());
        json.addProperty("type", "IMG");
        String token = preferences.getString(Config.BEAR_TOKEN, "");

        Ion.with(this)
                .load(Config.QUESTION_URL)
                .setTimeout(60 * 60 * 1000)
                .setHeader(Config.BEAR_TOKEN, preferences.getString(Config.BEAR_TOKEN, ""))
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e != null) {
                            Toast.makeText(getApplicationContext(), "Lỗi khi tải lên câu hỏi!", Toast.LENGTH_LONG).show();
                            return;
                        }else{
                            Intent intent = new Intent();
                            String json = gson.toJson(result);
                            intent.putExtra("newQuestion", json);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                });
    }
}
