package studio.sun.cybirst.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest ;
import com.android.volley.toolbox.Volley;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONObject;

import studio.sun.cybirst.R;
import studio.sun.cybirst.Utils.Config;

public class LoginActivity extends AppCompatActivity {
    EditText email;
    EditText password;
    Button login_button;
    AVLoadingIndicatorView avi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        avi = (AVLoadingIndicatorView)findViewById(R.id.avi);
        init();
        loginButton();
    }

    public void init() {
        email = (EditText) findViewById(R.id.editText_email);
        password = (EditText) findViewById(R.id.editText_password);
        login_button = (Button) findViewById(R.id.button_login);
    }

    public void loginButton() {
        login_button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        login(email.getText().toString().trim(), password.getText().toString().trim());
                    }
                }
        );
    }

    private void login(final String email, final String password){
        avi.show();
        login_button.setEnabled(false);
        //Creating a string request
        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", email);
        jsonParams.put("password", password);
        JsonObjectRequest postRequest = new JsonObjectRequest( Request.Method.POST, Config.LOGIN_URL,

                new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            String token = (String) response.getString("token");
                            Log.d("data", response.toString());
                            //Creating a shared preference
                            SharedPreferences sharedPreferences = LoginActivity.this.getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);

                            //Creating editor to store values to shared preferences
                            SharedPreferences.Editor editor = sharedPreferences.edit();

                            //Adding values to editor
                            editor.putString(Config.BEAR_TOKEN, token);

                            //Saving values to editor
                            editor.commit();

                            //Starting profile activity
                            Intent intent = new Intent(LoginActivity.this, QuestionsActivity.class);
                            startActivity(intent);
                            finish();
                        }catch(Exception e){
                            //Displaying an error message on toast
                            Toast.makeText(LoginActivity.this, "Email hoặc mật khẩu không đúng", Toast.LENGTH_LONG).show();
                            avi.hide();
                            login_button.setEnabled(true);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Handle Error
                        Log.d("data", error.toString());
                        Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        avi.hide();
                        login_button.setEnabled(true);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };

        //Adding the string request to the queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);
    }
}
