package studio.sun.cybirst.Entities;

import android.app.ActionBar;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import studio.sun.cybirst.R;

/**
 * Created by Sun on 10/17/2017.
 */

public class QuestionsAdapter extends ArrayAdapter<Question> {
    Context context;
    public QuestionsAdapter(Context context, ArrayList<Question> questions) {
        super(context, 0, questions);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Question question = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_question, parent, false);
        }
        // Lookup view for data population
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        TextView tvQuestionOwnerFullName = (TextView) convertView.findViewById(R.id.tvQuestionOwnerFullName);
        ImageView ivQuestionOwnerAvatar = (ImageView) convertView.findViewById(R.id.ivQuestionOwnerAvatar);
        ImageView ivContent = (ImageView) convertView.findViewById(R.id.ivContent);
        TextView tvQuestionNumAnswer = (TextView) convertView.findViewById(R.id.tvQuestionNumAnswer);
        LinearLayout llTags = (LinearLayout) convertView.findViewById(R.id.llTags);

        tvTitle.setText(question.getTitle());
        tvQuestionOwnerFullName.setText(question.getUser().getFullname());
        tvQuestionNumAnswer.setText(question.getNum_answer() + " trả lời");

        Picasso.with(this.getContext()).load(question.getUser().getAvatar()).fit().centerCrop()
                .error(R.mipmap.ic_launcher)
                .into(ivQuestionOwnerAvatar);

        Picasso.with(this.getContext()).load(question.getContent()).fit().centerCrop()
                .error(R.mipmap.ic_launcher)
                .into(ivContent);

        ArrayList<Tag> arrayList = question.getTags();
        int index = 0;
        llTags.removeAllViews();
        for (Tag tag : arrayList) {
            TextView tvTag = new TextView(convertView.getContext());
            tvTag.setId(index);
            tvTag.setText("(" + tag.getContent() + ")");
            tvTag.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            llTags.addView(tvTag);
            index++;
        }

        // Return the completed view to render on screen
        return convertView;
    }
}