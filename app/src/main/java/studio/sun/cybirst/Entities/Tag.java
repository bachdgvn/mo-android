package studio.sun.cybirst.Entities;

/**
 * Created by Sun on 10/17/2017.
 */

public class Tag {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String content;
    private String description;

    public Tag(int id,
               String content,
               String description) {
        this.id = id;
        this.content = content;
        this.description = description;
    }

    @Override
    public String toString() {
        return this.content;
    }
}
