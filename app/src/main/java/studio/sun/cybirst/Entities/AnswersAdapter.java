package studio.sun.cybirst.Entities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import studio.sun.cybirst.R;

/**
 * Created by Sun on 10/17/2017.
 */

public class AnswersAdapter extends ArrayAdapter<Answer> {
    Context context;
    public AnswersAdapter(Context context, ArrayList<Answer> answers) {
        super(context, 0, answers);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Answer answer = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_answer, parent, false);
        }
        // Lookup view for data population
        TextView tvAnswerOwnerFullName = (TextView) convertView.findViewById(R.id.tvAnswerOwnerFullName);
        TextView tvAnswerTitle = (TextView) convertView.findViewById(R.id.tvAnswerTitle);
        ImageView ivAnswerOwnerAvatar = (ImageView) convertView.findViewById(R.id.ivAnswerOwnerAvatar);
        ImageView ivContent = (ImageView) convertView.findViewById(R.id.ivContent);

        tvAnswerOwnerFullName.setText(answer.getUser().getFullname());
        tvAnswerTitle.setText(answer.getTitle());

        Picasso
            .with(context)
            .setLoggingEnabled(true);

        Picasso.with(this.getContext()).load(answer.getUser().getAvatar()).fit().centerCrop()
                .error(R.mipmap.ic_launcher)
                .into(ivAnswerOwnerAvatar);

        Picasso.with(this.getContext()).load(answer.getContent()).fit().centerCrop()
                .error(R.mipmap.ic_launcher)
                .into(ivContent);

        // Return the completed view to render on screen
        return convertView;
    }
}