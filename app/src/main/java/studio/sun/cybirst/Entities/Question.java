package studio.sun.cybirst.Entities;

import java.lang.reflect.Array;
import java.util.ArrayList;

import studio.sun.cybirst.Entities.Tag;
import studio.sun.cybirst.Entities.User;

/**
 * Created by Sun on 10/17/2017.
 */

public class Question {
    private String access_id;
    private String source_id;
    private String source;
    private String title;
    private String content;
    private int bounty_score;
    private Double added_time;
    private Double edited_time;
    private int num_answer;
    private int num_upvote;
    private int num_downvote;
    private int num_star;
    private int num_view;
    private int num_share;
    private int num_report;
    private int user_id;
    private String status;
    private ArrayList<Tag> tags;
    private User user;

    public String getAccess_id() {
        return access_id;
    }

    public void setAccess_id(String access_id) {
        this.access_id = access_id;
    }

    public String getSource_id() {
        return source_id;
    }

    public void setSource_id(String source_id) {
        this.source_id = source_id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getBounty_score() {
        return bounty_score;
    }

    public void setBounty_score(int bounty_score) {
        this.bounty_score = bounty_score;
    }

    public Double getAdded_time() {
        return added_time;
    }

    public void setAdded_time(Double added_time) {
        this.added_time = added_time;
    }

    public Double getEdited_time() {
        return edited_time;
    }

    public void setEdited_time(Double edited_time) {
        this.edited_time = edited_time;
    }

    public int getNum_answer() {
        return num_answer;
    }

    public void setNum_answer(int num_answer) {
        this.num_answer = num_answer;
    }

    public int getNum_upvote() {
        return num_upvote;
    }

    public void setNum_upvote(int num_upvote) {
        this.num_upvote = num_upvote;
    }

    public int getNum_downvote() {
        return num_downvote;
    }

    public void setNum_downvote(int num_downvote) {
        this.num_downvote = num_downvote;
    }

    public int getNum_star() {
        return num_star;
    }

    public void setNum_star(int num_star) {
        this.num_star = num_star;
    }

    public int getNum_view() {
        return num_view;
    }

    public void setNum_view(int num_view) {
        this.num_view = num_view;
    }

    public int getNum_share() {
        return num_share;
    }

    public void setNum_share(int num_share) {
        this.num_share = num_share;
    }

    public int getNum_report() {
        return num_report;
    }

    public void setNum_report(int num_report) {
        this.num_report = num_report;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Question(
            String access_id,
            String source_id,
            String source,
            String title,
            String content,
            int bounty_score,
            Double added_time,
            Double edited_time,
            int num_answer,
            int num_upvote,
            int num_downvote,
            int num_star,
            int num_view,
            int num_share,
            int num_report,
            int user_id,
            String status,
            ArrayList<Tag> tags,
            User user
    ) {
        this.access_id = access_id;
        this.source_id = source_id;
        this.source = source;
        this.title = title;
        this.content = content;
        this.bounty_score = bounty_score;
        this.added_time = added_time;
        this.edited_time = edited_time;
        this.num_answer = num_answer;
        this.num_upvote = num_upvote;
        this.num_downvote = num_downvote;
        this.num_star = num_star;
        this.num_view = num_view;
        this.num_share = num_share;
        this.num_report = num_report;
        this.user_id = user_id;
        this.status = status;
        this.tags = tags;
        this.user = user;
    }
}
