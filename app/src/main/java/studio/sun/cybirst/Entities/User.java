package studio.sun.cybirst.Entities;

/**
 * Created by Sun on 10/17/2017.
 */

public class User {
    private String id;
    private String username;
    private String fullname;
    private String avatar;
    private String description;
    private String tag;
    private Integer reputation_score;
    private Double joined_time;
    private Integer profile_views;
    private String status;
    private String gender;

    public User(String id,
                String username,
                String fullname,
                String avatar,
                String description,
                String tag,
                Integer reputation_score,
                Double joined_time,
                Integer profile_views,
                String status,
                String gender) {
        this.id = id;
        this.username = username;
        this.fullname = fullname;
        this.avatar = avatar;
        this.description = description;
        this.tag = tag;
        this.reputation_score = reputation_score;
        this.joined_time = joined_time;
        this.profile_views = profile_views;
        this.status = status;
        this.gender = gender;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getReputation_score() {
        return reputation_score;
    }

    public void setReputation_score(Integer reputation_score) {
        this.reputation_score = reputation_score;
    }

    public Double getJoined_time() {
        return joined_time;
    }

    public void setJoined_time(Double joined_time) {
        this.joined_time = joined_time;
    }

    public Integer getProfile_views() {
        return profile_views;
    }

    public void setProfile_views(Integer profile_views) {
        this.profile_views = profile_views;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
