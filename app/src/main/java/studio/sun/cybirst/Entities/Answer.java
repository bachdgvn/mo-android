package studio.sun.cybirst.Entities;

import java.util.ArrayList;

/**
 * Created by Sunchelsea on 12/13/2017.
 */

public class Answer {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccess_id() {
        return access_id;
    }

    public void setAccess_id(String access_id) {
        this.access_id = access_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Double getAdded_time() {
        return added_time;
    }

    public void setAdded_time(Double added_time) {
        this.added_time = added_time;
    }

    public Double getEdited_time() {
        return edited_time;
    }

    public void setEdited_time(Double edited_time) {
        this.edited_time = edited_time;
    }

    public int getNum_upvote() {
        return num_upvote;
    }

    public void setNum_upvote(int num_upvote) {
        this.num_upvote = num_upvote;
    }

    public int getNum_downvote() {
        return num_downvote;
    }

    public void setNum_downvote(int num_downvote) {
        this.num_downvote = num_downvote;
    }

    public int getNum_share() {
        return num_share;
    }

    public void setNum_share(int num_share) {
        this.num_share = num_share;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private int id;
    private String access_id;
    private String title;
    private String content;
    private Double added_time;
    private Double edited_time;
    private int num_upvote;
    private int num_downvote;
    private int num_share;
    private int user_id;
    private int question_id;
    private String status;
    private String type;
    private User user;

    public Answer(
            int id,
            String access_id,
            String content,
            Double added_time,
            Double edited_time,
            int num_upvote,
            int num_downvote,
            int num_share,
            int user_id,
            int question_id,
            String status,
            String type,
            User user
    ) {
        this.id = id;
        this.access_id = access_id;
        this.content = content;
        this.added_time = added_time;
        this.edited_time = edited_time;
        this.num_upvote = num_upvote;
        this.num_downvote = num_downvote;
        this.num_share = num_share;
        this.user_id = user_id;
        this.question_id = question_id;
        this.status = status;
        this.type = type;
        this.user = user;
    }
}
